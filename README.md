Storage extension for Naut

<https://en.wikipedia.org/wiki/Uluburun_shipwreck>

manual prereqs:

```sh
cmake (brew install cmake)
zstd (bash -c "$(curl -fsSL https://raw.githubusercontent.com/horta/zstd.install/main/install)")
```

Otherwise, at least on OSX, python's wrapper on wiredtiger won't install.

**PERFORMANCE**

The present implementation of Write and Read is really slow. There needs to be a way
to use a with style syntax and safely leave the db connection open for the life of a graph. But, until there is some way to react to a graph closure, this will just be slow....
