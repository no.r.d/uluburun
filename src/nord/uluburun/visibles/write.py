"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.utils.verboser import funlog


class Write(VisNode):
    """Implement the visualization.

    NOTE: There needs to be a check on name change, to ensure that
    no other writing Node has the same name .... or,
    Perhaps that is OK, if we want to modify the same data
    or table without instancing?
    """

    def __init__(self, uid, app):
        super().__init__(uid, 'write', app, space="uluburun")

        self.add_delegate("set_index_position",
                          "setindexposition",
                          'pass',
                          'as_tgt',
                          tiptext=_("Pass storage\nindex here"))  # noqa: F821

    @funlog
    def update_tooltip(self, tooltip):
        if hasattr(self, 'mapentry'):
            ttstring = f"Writing to: {self.mapentry.get_name()}"

            tooltip.set_text(f"{ttstring}")
            return
        tooltip.set_text(f"{self.name}: " + _("** Unset **"))  # noqa: F821
