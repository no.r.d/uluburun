"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.utils.verboser import funlog


class Exists(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        super().__init__(uid, 'exists', app, space="uluburun")

        self.add_delegate("set_index_position",
                          "setindexposition",
                          'pass',
                          'as_tgt',
                          tiptext=_("Pass storage\nindex here"))  # noqa: F821

    @funlog
    def update_tooltip(self, tooltip):
        if hasattr(self, 'mapentry'):
            tooltip.set_text(f"{self} ")
            return
        tooltip.set_text(f"{self.name}: " + _("** Unset **"))  # noqa: F821

    @funlog
    def connect_allowed(self, other, ctype, direction, delegate=None):
        """
        Do not allow connections of type pass to attach to this node directly.

        Only delegates can be passed to. However, delegates are resolved
        after graph/map load. So, in order to prevent, live-edit time
        connections, only check when called by the delegate (which means)
        that delegate will not be none.
        """
        if direction == 'as_tgt' and delegate is not None:
            if ctype == 'pass':
                print(delegate.uid)
                if delegate.uid.split(':')[0] in ('set_index_position'):
                    return True
                else:
                    return False
            else:
                return False
        else:
            return True
