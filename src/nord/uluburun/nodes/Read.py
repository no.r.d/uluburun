"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Node as Node
import sys
from nord.uluburun.utils.wiredtiger import Store
from hashlib import blake2s
from nord.uluburun.exceptions import StorageException


class Read(Node):
    """"""
    def set_index_position(self, rule):
        """This is called by the edge.apply_delegate via object interrogation."""
        if not hasattr(self, '_index'):
            self._index = rule.edge.get_cargo()

    def set_payload(self, payload):
        self._payload = payload

    def set_metadata(self, metadata):
        self._metadata = metadata

    def get_payload(self):
        if hasattr(self, '_payload'):
            return self._payload
        else:
            return None

    def has_payload(self):
        return hasattr(self, '_payload')

    def get_metadata(self):
        if hasattr(self, '_metadata'):
            return self._metadata
        else:
            return None

    def execute(self, runtime):
        idx = 0
        # get index value from decoration, if set...
        if hasattr(self, '_index'):
            idx = self._index
            # del self._index

        table = self.get_name()

        storepath = f"{blake2s(runtime.get_map().get_id().encode()).hexdigest()}"

        if not hasattr(self, '_data_store'):
            self._data_store = Store(storepath, table)

        metadata = self._data_store.getmeta(str(idx))
        if metadata is None:
            raise StorageException("Requested data does not exist")

        payload = self._data_store.get(str(idx))
        self.set_payload(payload)
        metadata = self._data_store.getmeta(str(idx))
        self.set_metadata(metadata)
        self.flag_exe(runtime)

    def next_after_execute(self, runtime, verbose, affect_schedule=True):
        del self._index
        super().next_after_execute(runtime, verbose, affect_schedule)


sys.modules[__name__] = Read
