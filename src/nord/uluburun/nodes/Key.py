"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Node as Node
import nord.nord.exceptions.RuntimeException as RuntimeException
import sys


class Key(Node):
    """"""
    def execute(self, runtime):
        pass
        raise RuntimeException("Not Implemented")
        self.flag_exe(runtime)
        return

        """
        https://stackoverflow.com/questions/36117046/cryptography-module-is-fernet-safe-and-can-i-do-aes-encryption-with-that-module
        """
        import secrets
        from cryptography.hazmat.primitives.ciphers.aead import AESGCM

        # Generate a random secret key (AES256 needs 32 bytes)
        key = secrets.token_bytes(32)

        # Encrypt a message
        nonce = secrets.token_bytes(12)  # GCM mode needs 12 fresh bytes every time
        ciphertext = nonce + AESGCM(key).encrypt(nonce, b"Message", b"")

        # Decrypt (raises InvalidTag if using wrong key or corrupted ciphertext)
        msg = AESGCM(key).decrypt(ciphertext[:12], ciphertext[12:], b"")


sys.modules[__name__] = Key
