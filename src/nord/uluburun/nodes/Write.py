"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Node as Node
import sys
from nord.uluburun.utils.wiredtiger import Store
import datetime
from hashlib import blake2s
from nord.uluburun.exceptions import StorageException


class Write(Node):
    """"""
    def set_index_position(self, rule):
        """This is called by the edge.apply_delegate via object interrogation."""
        if not hasattr(self, '_index'):
            self._index = rule.edge.get_cargo()

    def set_source_name(self, name):
        """
        This function should be called by the rules providing data
        such that the metadata can be recorded
        """
        self._source_name = name

    def set_source_id(self, id):
        """
        This function should be called by the rules providing data
        such that the metadata can be recorded
        """
        self._source_id = id

    def set_payload(self, payload):
        self._payload = payload

    def get_payload(self):
        if hasattr(self, '_payload'):
            return self._payload
        else:
            return None

    def execute(self, runtime):
        # check for payload
        # check to see if storage connection exists
        # if not, connect to storage
        # serialize the data (using to_dict and dumps?)
        # write the data to the connection
        # close the connection

        # it would be nice to store some metadata with the
        # payload.
        # things like:
        #      dt_created, dt_modified, dt_accessed
        #      source_node_id
        #      source_node_name
        #      nord version
        #      permissions (or something like it)
        #      created by

        idx = 0
        # get index value from decoration, if set...
        if hasattr(self, '_index'):
            idx = self._index
            del self._index
        payload = self.get_payload()
        if payload is None:
            raise StorageException("Cannot execute Write without passed data")
        table = self.get_name()

        storepath = f"{blake2s(runtime.get_map().get_id().encode()).hexdigest()}"

        if not hasattr(self, '_data_store'):
            self._data_store = Store(storepath, table)

        metadata = self._data_store.getmeta(str(idx))
        if metadata is None:
            dt_created = datetime.datetime.now().isoformat()

            metadata = {
                "dt_created": dt_created,
                "identity": "<UNKNOWN>",
                "permissions": "---------"
            }

        metadata["dt_modified"] = datetime.datetime.now().isoformat()
        metadata["version"] = runtime.get_version_string()
        metadata["source_name"] = self._source_name
        metadata["source_id"] = self._source_id

        self._data_store.set(str(idx), payload, metadata)
        self.flag_exe(runtime)


sys.modules[__name__] = Write
