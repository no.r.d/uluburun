"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Node as Node
from nord.uluburun.utils.wiredtiger import Store
from hashlib import blake2s
import sys


class Remove(Node):
    def set_index_position(self, rule):
        """This is called by the edge.apply_delegate via object interrogation."""
        if not hasattr(self, '_index'):
            self._index = rule.edge.get_cargo()

    def execute(self, runtime):
        idx = 0
        # get index value from decoration, if set...
        if hasattr(self, '_index'):
            idx = self._index
            # del self._index

        table = self.get_name()

        storepath = f"{blake2s(runtime.get_map().get_id().encode()).hexdigest()}"

        if not hasattr(self, '_data_store'):
            self._data_store = Store(storepath, table)

        self._data_store.remove(idx)

        self.flag_exe(runtime)


sys.modules[__name__] = Remove
