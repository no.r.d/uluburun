"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class PassWrite(Rule):
    """
    The PassWrite rule
    """

    def apply(self):
        """
        Takes the cargo off the edge and adds it to the target builtin's
        set of arguments.
        """
        if self.edge.has_cargo():
            if hasattr(self.edge, 'delegate'):
                return self.edge.apply_delegate(self)
            else:
                tgt = self.edge.get_target()
                payload = self.edge.get_cargo()
                """
                Items which may need to be made friendly for storage (like a sushrut:Dataobject)
                will implement the get_storage_content, and apply_storage_content methods
                """
                if hasattr(payload, "get_storage_content"):
                    payload = payload.get_storage_content()
                tgt.set_payload(payload)
                tgt.set_source_name(self.edge.get_source().get_name())
                tgt.set_source_id(self.edge.get_source().get_id())
                return True
        else:
            raise RuntimeException(_("Write target of pass, but nothing passed: {}").format(self.edge.get_target()))  # noqa: F821, E501


sys.modules[__name__] = PassWrite
