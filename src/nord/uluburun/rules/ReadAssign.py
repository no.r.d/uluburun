"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Rule as Rule
import sys


class ReadAssign(Rule):
    """
    The ReadAssign rule
    """

    def apply(self):
        """
        Get's the payload from the source and places it as
        cargo on the edge.
        """

        if hasattr(self.edge, 'delegate'):
            return self.edge.apply_delegate(self)
        else:
            src = self.edge.get_source()
            if src.has_payload():
                self.edge.set_cargo(src.get_payload())
                return True
        return False


sys.modules[__name__] = ReadAssign
