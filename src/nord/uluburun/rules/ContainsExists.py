"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class ContainsExists(Rule):
    """
    The ContainsExists rule
    """

    def apply(self):
        """
        Takes the cargo off the edge and adds it to the target builtin's
        set of arguments.
        """
        raise RuntimeException(_("NOT IMPLEMENTED: {} ContainsExists {}").format(self.edge.get_source(), self.edge.get_target()))  # noqa: F821, E501

        # Sample code below:

        if self.edge.has_cargo():
            if hasattr(self.edge, 'delegate'):
                self.edge.apply_delegate(self)
            else:
                self.edge.get_target().set_arg(self.edge, self.edge.get_cargo())
            return True
        else:
            raise RuntimeException(_("Delete target of pass, but nothing passed: {}").format(self.edge.get_target()))  # noqa: F821, E501


sys.modules[__name__] = ContainsExists
