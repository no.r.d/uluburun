"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from wiredtiger import wiredtiger_open
from pathlib import Path
import json
import datetime
from nord.uluburun.exceptions import StorageException

WT_NOT_FOUND = -31803


class SharedConn():
    def __init__(self, directory, table):
        self.use_count = 0
        self.table = table
        self.directory = directory
        # need to ensure that the directory exists
        spath = Path(directory)
        if not spath.exists():
            spath.mkdir()

    def open(self):
        if self.use_count == 0:

            self.conn = wiredtiger_open(self.directory, 'create,log=(enabled,recover=on),statistics=(all)')
            self.session = self.conn.open_session()

        self.use_count += 1
        return self.conn, self.session

    def close(self):
        self.use_count -= 1
        if self.use_count <= 1:
            self.conn.close()

    def __del__(self):
        self.conn.close()


class Store():

    _connections = {}

    @staticmethod
    def add_connection(directory, table):
        Store._connections[directory] = SharedConn(directory, table)

    @staticmethod
    def has_connection(directory, table):
        return directory in Store._connections

    @staticmethod
    def rem_connection(directory, table):
        if Store.has_connection(directory, table):
            del Store._connections[directory]

    @staticmethod
    def get_connection(directory, table):
        if not Store.has_connection(directory, table):
            Store.add_connection(directory, table)
        return Store._connections[directory]

    def __init__(self, directory, table, retain_conn=True):
        self.is_open = False
        self.directory = directory
        self.table = table
        self.connection, self.session = Store.get_connection(directory, table).open()

        tpath = Path(self.directory).joinpath(f"{self.table}.wt")
        if not tpath.exists():
            """
            Create the table, if it's data file does not exist

            NOTE: THIS IS FRAGILE. If later versions of WT change names, etc...

            But, there does not appear to be a lightweight way to see if the
            table already exists...
            """
            self.session.begin_transaction()
            # the first table creation is the actual data table
            self.session.create(f"table:{self.table}", "key_format=S,value_format=S")
            # the second table il for the metadata
            self.session.create(f"table:..{self.table}", "key_format=S,value_format=S")
            self.session.commit_transaction()

        self.cursor = self.session.open_cursor(f"table:{self.table}")
        self.metacursor = self.session.open_cursor(f"table:..{self.table}")
        self.is_open = True
        self.retain_conn = retain_conn
        if not self.retain_conn:
            self.close()

    def __exit__(self, *args, **kwargs):
        self.close()

    def __enter__(self):
        return self

    def __del__(self):
        self.close()

    def _open(self):
        if not self.is_open:
            self.connection, self.session = Store.get_connection(self.directory, self.table).open()
            self.cursor = self.session.open_cursor(f"table:{self.table}")
            self.metacursor = self.session.open_cursor(f"table:..{self.table}")
            self.is_open = True

    def close(self):
        if self.is_open:
            Store.get_connection(self.directory, self.table).close()
        self.is_open = False

    def get(self, key):
        self._open()
        self.session.begin_transaction()
        self.cursor.set_key(key)
        self.metacursor.set_key(key)
        if self.cursor.search() == WT_NOT_FOUND:
            self.session.rollback_transaction()
            raise StorageException(f"No data found in {self.table} for key: {key}")
        if self.metacursor.search() == WT_NOT_FOUND:
            self.session.rollback_transaction()
            raise StorageException(f"No metadata found for {self.table} for key: {key}")
        sval = self.cursor.get_value()
        mval = self.metacursor.get_value()
        md = json.loads(mval)
        md["dt_accessed"] = datetime.datetime.now().isoformat()
        self.metacursor.set_value(json.dumps(md))
        self.metacursor.insert()
        self.session.commit_transaction()
        if not self.retain_conn:
            self.close()
        return json.loads(sval)

    def getmeta(self, key):
        self._open()
        self.session.begin_transaction()
        self.metacursor.set_key(key)
        if self.metacursor.search() == WT_NOT_FOUND:
            self.session.commit_transaction()
            return None
        mval = self.metacursor.get_value()
        md = json.loads(mval)
        self.session.commit_transaction()
        if not self.retain_conn:
            self.close()
        return md

    def set(self, key, value, metadata):
        self._open()
        self.session.begin_transaction()
        self.cursor.set_key(key)
        self.cursor.set_value(json.dumps(value))
        self.metacursor.set_key(key)
        self.metacursor.set_value(json.dumps(metadata))
        self.cursor.insert()
        self.metacursor.insert()
        self.session.commit_transaction()
        if not self.retain_conn:
            self.close()

    def exists(self, key):
        self._open()
        self.session.begin_transaction()
        self.cursor.set_key(key)
        self.metacursor.set_key(key)
        f1 = not self.cursor.search() == WT_NOT_FOUND
        f2 = not self.metacursor.search() == WT_NOT_FOUND
        self.session.commit_transaction()
        if not self.retain_conn:
            self.close()
        if f1 and f2:
            return True
        if not f1 and not f2:
            return False
        if f1 or f2:
            raise StorageException(f"Inconsistent data state for {self.directory}/{self.table}[{key}]")

    def remove(self, key):
        self._open()
        self.session.begin_transaction()
        self.cursor.set_key(key)
        self.metacursor.set_key(key)
        if self.cursor.search() == WT_NOT_FOUND:
            raise StorageException(f"No data to delete for {self.directory}/{self.table}[{key}]")
        if self.metacursor.search() == WT_NOT_FOUND:
            raise StorageException(f"No metadata to delete for {self.directory}/{self.table}[{key}]")
        self.metacursor.remove()
        self.cursor.remove()
        self.session.commit_transaction()
        if not self.retain_conn:
            self.close()
