#!/usr/bin/env bash
if [ ${#} -ne 1 ]; then
    echo "Must pass one, ond only one argument on the command line."
    exit 1
fi

EDGE="${1}"
EDGE=$(echo ${EDGE} | tr '[:upper:]' '[:lower:]')
CEDGE=$(echo $(tr a-z A-Z <<< ${EDGE:0:1})${EDGE:1})
SPACE=$(basename $(pwd))

cat <<EOF > src/nord/${SPACE}/edges/${CEDGE}.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Edge as Edge
import sys


class ${CEDGE}(Edge):
    """"""
    pass


sys.modules[__name__] = ${CEDGE}
EOF

cat <<EOF > tests/edges/${CEDGE}_test.py
"""
Copyright $(date +%Y) NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.${SPACE}.edges.${CEDGE} as ${CEDGE}
import nord.nord.Map as Map


def test_${CEDGE}():
    a = ${CEDGE}(Map(None))
    assert repr(a) == '${CEDGE}: [None]  --${CEDGE}-->  [None]'

EOF

if ! grep -q \<${EDGE}\> src/nord/${SPACE}/config/modelmap.conf $2>&1; then
    echo "${EDGE}:models!thingreytransedge.egg" >> src/nord/${SPACE}/config/modelmap.conf
fi

if ! grep -q contains:flow:block src/nord/${SPACE}/config/visprecedence.conf $2>&1; then
    echo "contains:flow:block" >> src/nord/${SPACE}/config/visprecedence.conf
fi

if ! grep -q \<${EDGE}\> src/nord/${SPACE}/config/vismap.conf $2>&1; then
    echo "${EDGE}:nord.sigurd.managers.connectionmanager.draw_${EDGE}" >> src/nord/${SPACE}/config/vismap.conf
fi

echo ${0} ${*} >> .mk_skel_gen.log