"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
from nord.nord import Runtime as RT
import nord.nord.Map as Map
import nord.uluburun.nodes.Key as Key
import nord.uluburun.nodes.Write as Write
import nord.uluburun.edges.Lock as Lock
Runtime = RT.Runtime


# RIGHT

def test_KeyLock_Write():
    Na = Key(Map(None))
    Na.id = 1
    Nb = Write(Map(None))
    Nb.id = 2
    rt = Runtime()
    e = Lock(Map(None))
    e.set_nodes(Na, Nb, 'Lock')
    e.follow(rt)

    assert repr(e) == "Lock: [Node(Key): <id: 1>]  --Lock-->  [Node(Write): <id: 2>]**"

