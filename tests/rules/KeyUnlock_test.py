"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
from nord.nord import Runtime as RT
import nord.nord.Map as Map
import nord.uluburun.nodes.Key as Key
import nord.uluburun.nodes.Read as Read
import nord.uluburun.edges.Unlock as Unlock
Runtime = RT.Runtime


# RIGHT

def test_KeyUnlock_Read():
    Na = Key(Map(None))
    Na.id = 1
    Nb = Read(Map(None))
    Nb.id = 2
    rt = Runtime()
    e = Unlock(Map(None))
    e.set_nodes(Na, Nb, 'Unlock')
    e.follow(rt)

    assert repr(e) == "Unlock: [Node(Key): <id: 1>]  --Unlock-->  [Node(Read): <id: 2>]**"

