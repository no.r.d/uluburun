"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from nord.uluburun.utils.wiredtiger import Store
from nord.uluburun.exceptions import StorageException
import pathlib as p
import shutil
import pytest


@pytest.fixture(scope="function")
def wt_wrkdir():
    wrkdir = './.uluburun_wt_tests'
    wp = p.Path(wrkdir)
    if not wp.exists():
        wp.mkdir()
    yield wrkdir
    shutil.rmtree(wp)


def test_open_store(wt_wrkdir):
    wrkdir = wt_wrkdir
    e = None
    try:
        s1 = Store(wrkdir, 'tt1', retain_conn=False)
        s2 = Store(wrkdir, 'tt2', retain_conn=False)
        s1.close()
        s2.close()
    except Exception as ee:
        e = ee

    assert e is None


def test_set_and_get(wt_wrkdir):
    wrkdir = wt_wrkdir
    e = None
    try:
        s1 = Store(wrkdir, 'tt1', retain_conn=False)
        s2 = Store(wrkdir, 'tt2', retain_conn=False)

        missing_key = False
        try:
            s1.get('1234')
        except StorageException:
            missing_key = True

        assert missing_key

        s1.set('1', {"a": 1, "b": 2}, {"a": 'NA'})
        s2.set('1', {"a": 1, "b": 2}, {"a": 'NA'})
        s1.set('1', {"a": 3, "b": 4}, {"a": 'NA'})
        s1.set('2', {"a": 5, "b": 6}, {"a": 'NA'})

        t1_1 = s1.get('1')
        t1_2 = s1.get('2')
        t2_1 = s2.get('1')

        assert t1_1 == {"a": 3, "b": 4}
        assert t1_2 == {"a": 5, "b": 6}
        assert t2_1 == {"a": 1, "b": 2}

    except Exception as ee:
        e = ee

    assert e is None


def test_set_and_get_keep_open(wt_wrkdir):
    wrkdir = wt_wrkdir
    e = None
    try:
        s1 = Store(wrkdir, 'tt1', retain_conn=True)
        s2 = Store(wrkdir, 'tt2', retain_conn=True)

        missing_key = False
        try:
            s1.get('1234')
        except StorageException:
            missing_key = True

        assert missing_key

        s1.set('1', {"a": 1, "b": 2}, {"a": 'NA'})
        s2.set('1', {"a": 1, "b": 2}, {"a": 'NA'})
        s1.set('1', {"a": 3, "b": 4}, {"a": 'NA'})
        s1.set('2', {"a": 5, "b": 6}, {"a": 'NA'})

        t1_1 = s1.get('1')
        t1_2 = s1.get('2')
        t2_1 = s2.get('1')

        assert t1_1 == {"a": 3, "b": 4}
        assert t1_2 == {"a": 5, "b": 6}
        assert t2_1 == {"a": 1, "b": 2}

    except Exception as ee:
        e = ee

    assert e is None


def test_exists(wt_wrkdir):
    wrkdir = wt_wrkdir
    s1 = Store(wrkdir, 'tt1', retain_conn=False)
    assert s1.exists('larry') is False

    s1.set('larry', {"a": 2, "B": 33}, {"meta": "NA"})
    assert s1.exists('larry')


def test_remove(wt_wrkdir):
    wrkdir = wt_wrkdir
    s1 = Store(wrkdir, 'tt1', retain_conn=False)
    assert s1.exists('larry') is False

    s1.set('larry', {"a": 2, "B": 33}, {"c": 1234})
    assert s1.exists('larry')

    s1.remove("larry")
    assert s1.exists('larry') is False
