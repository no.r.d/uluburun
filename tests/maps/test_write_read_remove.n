{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir://."
                ],
                "path": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://",
                    "dir://."
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "uluburun": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "id": "dbf87878-496f-4fc1-9ea7-fbfef86b2545",
    "map": {
        "edges": [
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "23d19f5a-af54-4b36-b063-fe9184a6e712:aaa",
                "space": "nord",
                "target": "26f412f0-816a-4193-ba33-71f52217f05f:table1",
                "to_space": "uluburun"
            },
            {
                "from_space": "uluburun",
                "rule": "pass",
                "source": "26f412f0-816a-4193-ba33-71f52217f05f:table1",
                "space": "nord",
                "target": "e6f6e4b0-5da7-43d1-afb3-abb60d24b6e0:Cuddly Pounded Table",
                "to_space": "kamal"
            },
            {
                "from_space": "sushrut",
                "rule": "flow",
                "source": "2f66e13e-d6be-4ad9-86e8-4a000109321b:Sensitive Pounded Scottsman",
                "space": "nord",
                "target": "63ec8332-a619-436e-bb2f-32088e2a6f19:aaa",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "4d1d4ab7-eb55-421b-8294-f27b1ff0abff:Found It",
                "space": "nord",
                "target": "d4cabc04-6952-485e-bdef-c55aa27400a2:Mild Chipped Catapult",
                "to_space": "nord"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "63ec8332-a619-436e-bb2f-32088e2a6f19:aaa",
                "space": "nord",
                "target": "746512ff-08c8-4b42-9be5-c2846c6793e2:table1",
                "to_space": "uluburun"
            },
            {
                "from_space": "uluburun",
                "rule": "flow",
                "source": "746512ff-08c8-4b42-9be5-c2846c6793e2:table1",
                "space": "nord",
                "target": "23d19f5a-af54-4b36-b063-fe9184a6e712:aaa",
                "to_space": "sushrut"
            },
            {
                "from_space": "uluburun",
                "rule": "assign",
                "source": "8261924e-20a0-407a-a2ef-dd8d9e68a45f:table1",
                "space": "nord",
                "target": "2f66e13e-d6be-4ad9-86e8-4a000109321b:Sensitive Pounded Scottsman",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "86e35a4d-6dc2-4c20-aa08-1af42af5b7b1:This is the payload",
                "space": "nord",
                "target": "989f0b80-dcc3-4e41-8df9-b7c047a3b7af:table1",
                "to_space": "uluburun"
            },
            {
                "from_space": "uluburun",
                "rule": "flow",
                "source": "989f0b80-dcc3-4e41-8df9-b7c047a3b7af:table1",
                "space": "nord",
                "target": "cc87c8af-f009-4880-ad59-d8a2a43b71af:aaa",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "9ceeb924-ff34-4dac-aa98-5eb7232e3fde:TRUE",
                "space": "nord",
                "target": "e6f6e4b0-5da7-43d1-afb3-abb60d24b6e0:Cuddly Pounded Table",
                "to_space": "kamal"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "cc87c8af-f009-4880-ad59-d8a2a43b71af:aaa",
                "space": "nord",
                "target": "8261924e-20a0-407a-a2ef-dd8d9e68a45f:table1",
                "to_space": "uluburun"
            },
            {
                "from_space": "kamal",
                "rule": "trueflow",
                "source": "e6f6e4b0-5da7-43d1-afb3-abb60d24b6e0:Cuddly Pounded Table",
                "space": "kamal",
                "target": "4d1d4ab7-eb55-421b-8294-f27b1ff0abff:Found It",
                "to_space": "sushrut"
            },
            {
                "from_space": "kamal",
                "rule": "falseflow",
                "source": "e6f6e4b0-5da7-43d1-afb3-abb60d24b6e0:Cuddly Pounded Table",
                "space": "kamal",
                "target": "f5da9cef-80c2-43b2-becb-f52ab2396163:Did not find it",
                "to_space": "sushrut"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "e8f9f217-6c3a-4915-ac03-2c1c6c25be3e:aaa",
                "space": "nord",
                "target": "989f0b80-dcc3-4e41-8df9-b7c047a3b7af:table1",
                "to_space": "uluburun"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "f5da9cef-80c2-43b2-becb-f52ab2396163:Did not find it",
                "space": "nord",
                "target": "6e35fe83-1b8c-47a7-9baf-4572aba3b768:Aloof Bifurcated Sound",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "23d19f5a-af54-4b36-b063-fe9184a6e712",
                "name": "aaa",
                "position": [
                    19.265209197998047,
                    3.9157333374023438,
                    3.637505531311035
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "aaa"
            },
            {
                "id": "26f412f0-816a-4193-ba33-71f52217f05f",
                "name": "table1",
                "position": [
                    19.41558074951172,
                    2.2600936889648438,
                    -2.7517433166503906
                ],
                "space": "uluburun",
                "type": "exists"
            },
            {
                "id": "2f66e13e-d6be-4ad9-86e8-4a000109321b",
                "name": "Sensitive Pounded Scottsman",
                "position": [
                    8.294624328613281,
                    2.2151870727539062,
                    -8.399527549743652
                ],
                "space": "sushrut",
                "type": "datastr",
                "value": "This is the payload"
            },
            {
                "id": "4d1d4ab7-eb55-421b-8294-f27b1ff0abff",
                "name": "Found It",
                "position": [
                    29.667877197265625,
                    3.94781494140625,
                    -5.2436981201171875
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "Found It"
            },
            {
                "id": "63ec8332-a619-436e-bb2f-32088e2a6f19",
                "name": "aaa",
                "position": [
                    14.071165084838867,
                    2.6373214721679688,
                    3.7720155715942383
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "aaa"
            },
            {
                "id": "6e35fe83-1b8c-47a7-9baf-4572aba3b768",
                "name": "Aloof Bifurcated Sound",
                "position": [
                    36.14833450317383,
                    1.5169754028320312,
                    -13.191740989685059
                ],
                "space": "nord",
                "type": "builtin"
            },
            {
                "id": "746512ff-08c8-4b42-9be5-c2846c6793e2",
                "name": "table1",
                "position": [
                    14.258848190307617,
                    2.802021026611328,
                    -2.5720627307891846
                ],
                "space": "uluburun",
                "type": "remove"
            },
            {
                "id": "8261924e-20a0-407a-a2ef-dd8d9e68a45f",
                "name": "table1",
                "position": [
                    8.190095901489258,
                    2.9186935424804688,
                    -2.7897708415985107
                ],
                "space": "uluburun",
                "type": "read"
            },
            {
                "id": "86e35a4d-6dc2-4c20-aa08-1af42af5b7b1",
                "name": "This is the payload",
                "position": [
                    -0.6155986785888672,
                    2.7013092041015625,
                    4.435122966766357
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "This is the payload"
            },
            {
                "id": "989f0b80-dcc3-4e41-8df9-b7c047a3b7af",
                "name": "table1",
                "position": [
                    -0.46351051330566406,
                    3.0906982421875,
                    -2.7491259574890137
                ],
                "space": "uluburun",
                "type": "write"
            },
            {
                "id": "9ceeb924-ff34-4dac-aa98-5eb7232e3fde",
                "name": "TRUE",
                "position": [
                    20.160301208496094,
                    1.8060951232910156,
                    -13.890154838562012
                ],
                "space": "sushrut",
                "type": "staticbool",
                "value": "TRUE"
            },
            {
                "id": "cc87c8af-f009-4880-ad59-d8a2a43b71af",
                "name": "aaa",
                "position": [
                    8.140430450439453,
                    2.7747764587402344,
                    3.663663387298584
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "aaa"
            },
            {
                "id": "d4cabc04-6952-485e-bdef-c55aa27400a2",
                "name": "Mild Chipped Catapult",
                "position": [
                    35.86532974243164,
                    0.749420166015625,
                    -5.528407573699951
                ],
                "space": "nord",
                "type": "builtin"
            },
            {
                "id": "e6f6e4b0-5da7-43d1-afb3-abb60d24b6e0",
                "name": "Cuddly Pounded Table",
                "position": [
                    25.427780151367188,
                    4.5893707275390625,
                    -9.28863525390625
                ],
                "space": "kamal",
                "type": "equal",
                "vis_exe_repr": "False = True"
            },
            {
                "id": "e8f9f217-6c3a-4915-ac03-2c1c6c25be3e",
                "name": "aaa",
                "position": [
                    -6.499698638916016,
                    0.144744873046875,
                    -2.5873029232025146
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "aaa"
            },
            {
                "id": "f5da9cef-80c2-43b2-becb-f52ab2396163",
                "name": "Did not find it",
                "position": [
                    29.412696838378906,
                    3.5112648010253906,
                    -13.186626434326172
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "Did not find it"
            }
        ]
    },
    "name": "Light Rounded War"
}