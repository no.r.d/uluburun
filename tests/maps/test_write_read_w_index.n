{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "uluburun": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "id": "6ff84b76-d05d-4c31-b943-6121454a7136",
    "map": {
        "edges": [
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "5eb8b7b8-331d-4997-a66e-9193ee31b923:22",
                "space": "nord",
                "target": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1:Sharp Flooded Sword",
                "to_space": "uluburun"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "6fce3abb-45b2-48ea-8316-9c8414faa30b:Read Index: 22",
                "space": "nord",
                "target": "71237010-886c-497e-8d47-ca13069ea730:Sharp Flooded Sword",
                "to_space": "uluburun"
            },
            {
                "from_space": "uluburun",
                "rule": "assign",
                "source": "71237010-886c-497e-8d47-ca13069ea730:Sharp Flooded Sword",
                "space": "nord",
                "target": "4164b2d8-2748-4cdc-985f-fabf3d2ed1e1:Sensitive Mischevious Bird",
                "to_space": "sushrut"
            },
            {
                "from_space": "uluburun",
                "rule": "flow",
                "source": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1:Sharp Flooded Sword",
                "space": "nord",
                "target": "f61b0d4f-34c9-4935-bc41-395e75d1c05d:Read Data From Storage",
                "to_space": "nord"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "f61b0d4f-34c9-4935-bc41-395e75d1c05d:Read Data From Storage",
                "space": "nord",
                "target": "4164b2d8-2748-4cdc-985f-fabf3d2ed1e1:Sensitive Mischevious Bird",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "f61b0d4f-34c9-4935-bc41-395e75d1c05d:Read Data From Storage",
                "space": "nord",
                "target": "6fce3abb-45b2-48ea-8316-9c8414faa30b:Read Index: 22",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "f61b0d4f-34c9-4935-bc41-395e75d1c05d:Read Data From Storage",
                "space": "nord",
                "target": "71237010-886c-497e-8d47-ca13069ea730:Sharp Flooded Sword",
                "to_space": "uluburun"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "f7ca98dc-1fec-4f72-8f7a-ef4dc93560d8:Hey! This is some test data!",
                "space": "nord",
                "target": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1:Sharp Flooded Sword",
                "to_space": "uluburun"
            }
        ],
        "nodes": [
            {
                "contents": {},
                "id": "4164b2d8-2748-4cdc-985f-fabf3d2ed1e1",
                "name": "Sensitive Mischevious Bird",
                "position": [
                    -6.6596221923828125,
                    0.8523826599121094,
                    -6.612029075622559
                ],
                "space": "sushrut",
                "type": "dataobject"
            },
            {
                "id": "5eb8b7b8-331d-4997-a66e-9193ee31b923",
                "name": "22",
                "position": [
                    2.876372814178467,
                    -0.8241081237792969,
                    7.169492244720459
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "22"
            },
            {
                "id": "6fce3abb-45b2-48ea-8316-9c8414faa30b",
                "name": "Read Index: 22",
                "position": [
                    11.501891136169434,
                    0.0,
                    -5.401017665863037
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "22"
            },
            {
                "id": "71237010-886c-497e-8d47-ca13069ea730",
                "name": "Sharp Flooded Sword",
                "position": [
                    2.9294424057006836,
                    -0.4892845153808594,
                    -6.449730396270752
                ],
                "space": "uluburun",
                "type": "read"
            },
            {
                "id": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1",
                "name": "Sharp Flooded Sword",
                "position": [
                    2.9406912326812744,
                    0.0,
                    1.8644756078720093
                ],
                "space": "uluburun",
                "type": "write"
            },
            {
                "id": "f61b0d4f-34c9-4935-bc41-395e75d1c05d",
                "name": "Read Data From Storage",
                "position": [
                    10.058128356933594,
                    0.0,
                    6.267846584320068
                ],
                "space": "nord",
                "type": "container"
            },
            {
                "id": "f7ca98dc-1fec-4f72-8f7a-ef4dc93560d8",
                "name": "Hey! This is some test data!",
                "position": [
                    -5.497200965881348,
                    -0.016788482666015625,
                    1.8993116617202759
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "Hey! This is some test data!"
            }
        ]
    },
    "name": "Sensitive Onerous Ceiling"
}