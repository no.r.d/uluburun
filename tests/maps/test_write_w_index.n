{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "uluburun": {
            "locations": {
                "code": [
                    "null://.",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun",
                    "pkg://",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord/uluburun/",
                    "dir:///Users/nate/Desktop/Dev/NoRD/uluburun/src/nord"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "id": "6ff84b76-d05d-4c31-b943-6121454a7136",
    "map": {
        "edges": [
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "069ebee2-6167-4187-904a-525271e08842:The data to be stored, will be assigned here",
                "space": "nord",
                "target": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1:Sharp Flooded Sword",
                "to_space": "uluburun"
            },
            {
                "delegate": {
                    "direction": "as_tgt",
                    "rule_type": "pass",
                    "uid": "set_index_position"
                },
                "from_space": "sushrut",
                "rule": "pass",
                "source": "5eb8b7b8-331d-4997-a66e-9193ee31b923:22",
                "space": "nord",
                "target": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1:Sharp Flooded Sword",
                "to_space": "uluburun"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "bd4c2d26-b43b-4b94-9c56-a3b4cccec879:This is the data to be stored",
                "space": "nord",
                "target": "069ebee2-6167-4187-904a-525271e08842:The data to be stored, will be assigned here",
                "to_space": "sushrut"
            }
        ],
        "nodes": [
            {
                "id": "0188e1e6-7f13-4ff4-aafa-412f0b66c049",
                "name": "1988",
                "position": [
                    15.757524490356445,
                    0.0,
                    -2.6931111812591553
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "1988"
            },
            {
                "id": "069ebee2-6167-4187-904a-525271e08842",
                "name": "The data to be stored, will be assigned here",
                "position": [
                    -6.048819541931152,
                    2.0452232360839844,
                    2.2949368953704834
                ],
                "space": "sushrut",
                "type": "datastr"
            },
            {
                "id": "34c75171-ec33-49ec-9d70-a8624b26fbff",
                "name": "String Value",
                "position": [
                    19.35477066040039,
                    0.0,
                    -2.5368762016296387
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "String Value"
            },
            {
                "id": "5eb8b7b8-331d-4997-a66e-9193ee31b923",
                "name": "22",
                "position": [
                    -4.850982666015625,
                    -0.8340415954589844,
                    6.8610992431640625
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "22"
            },
            {
                "id": "bd001898-dd2d-45f0-9ef2-b795a99c7bd1",
                "name": "Sharp Flooded Sword",
                "position": [
                    2.9406912326812744,
                    0.0,
                    1.8644756078720093
                ],
                "space": "uluburun",
                "type": "write"
            },
            {
                "id": "bd4c2d26-b43b-4b94-9c56-a3b4cccec879",
                "name": "This is the data to be stored",
                "position": [
                    -13.014083862304688,
                    -0.7101364135742188,
                    2.2517480850219727
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "This is the data to be stored"
            }
        ]
    },
    "name": "Sensitive Onerous Ceiling"
}