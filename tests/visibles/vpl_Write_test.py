"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""

import pytest
from panda3d.core import Point3
import nord.sigurd.shell.vismap as vm
from nord.wrigan.visibles.utils import dim_map, undim_map
import nord.sigurd.managers.objectmanager as om


@pytest.mark.panda3d()
def test_add_write_node(init_panda3d):
    # app = init_panda3d
    vm.create_node("Write", Point3(0.0, 0.0, 0.0), space="uluburun")
    karte = vm.get_working_map()
    # app.one_loop_iteration()
    r = render  # noqa: F821
    newNode = karte.nodes[list(karte.nodes.keys())[0]]
    dim_map()
    undim_map()
    # This assert detects if there are more NodePaths than there should be.
    # This was a problem detected when the Node Constructor was
    # called twice in the Static Node init method. Which in turn
    # caused the CDC capability to have two add_node changes
    # for the same node... which in turn, caused two model's to be
    # created with the same ID, which is bad...
    assert len(r.findAllMatches(f"{newNode.id}")) == 1
    assert om.fetch(newNode.id).data == r.findAllMatches(f"{newNode.id}")[0]

