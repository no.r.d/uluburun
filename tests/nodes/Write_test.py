"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.uluburun.nodes.Write as Write
import nord.nord.Map as Map
import json  # noqa: E402
import pytest  # noqa: E402, F401
import shutil
from hashlib import blake2s
from nord.uluburun.exceptions import StorageException
from nord.uluburun.utils.wiredtiger import Store
from nord.nord import Runtime as RT
Runtime = RT.Runtime


def test_write_run_gen():
    try:
        m = json.load(open("tests/maps/test_gen_node_write.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_gen_node_write.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    node = karte.get_node('c934b0fd149cc6103ee78604b536e11178bc1ade')
    assert isinstance(node, Write)
    exc = vessal.get_anomaly()
    assert isinstance(exc, StorageException)
    assert exc.args[0] == 'Cannot execute Write without passed data'


def test_write_data(cleanup_datastore):
    try:
        m = json.load(open("tests/maps/test_write_w_index.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_write_w_index.n", "r"))
    karte = Map(m)
    # schedule the removal of the wiredtiger directory
    storedir = f"{blake2s(karte.get_id().encode()).hexdigest()}"
    cleanup_datastore(lambda:  shutil.rmtree(storedir))

    vessal = Runtime()
    karte.run(vessal, None)

    assert vessal.get_anomaly() is None

    node = karte.get_node('bd001898-dd2d-45f0-9ef2-b795a99c7bd1')
    node._data_store.close()
    karte.unload()
    vessal = None
    karte = None
    del vessal
    del karte
    read_data = "Nope"
    with Store(storedir, "Sharp Flooded Sword") as st:
        read_data = st.get("22")
    assert read_data == "This is the data to be stored"
