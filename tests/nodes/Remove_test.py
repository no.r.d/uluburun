"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.uluburun.nodes.Remove as Remove  # noqa: F401
import nord.nord.Map as Map
from nord.nord import Runtime as RT
from hashlib import blake2s
import shutil
import json  # noqa: E402, F401
import pytest  # noqa: E402, F401
Runtime = RT.Runtime


def test_remove_run_gen():
    try:
        m = json.load(open("tests/maps/test_gen_node_remove.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_gen_node_remove.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    # node = karte.get_node('32ade10e30db599c61a43109a34dd7d7d2a0ebec')
    assert 1 == 1


def test_write_read_remove(cleanup_datastore, capsys):
    try:
        m = json.load(open("tests/maps/test_write_read_remove.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_write_read_remove.n", "r"))
    karte = Map(m)
    # schedule the removal of the wiredtiger directory
    storedir = f"{blake2s(karte.get_id().encode()).hexdigest()}"
    cleanup_datastore(lambda:  shutil.rmtree(storedir))

    vessal = Runtime()
    karte.run(vessal, None)

    assert vessal.get_anomaly() is None

    captured = capsys.readouterr()
    assert captured.out == "Did not find it\n"
