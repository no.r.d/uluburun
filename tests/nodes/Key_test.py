"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.uluburun.nodes.Key as Key
import nord.nord.Map as Map
from nord.nord import Runtime as RT
Runtime = RT.Runtime
import json  # noqa: E402
import pytest  # noqa: E402


def test_key_run_gen():
    try:
        m = json.load(open("tests/maps/test_gen_node_key.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_gen_node_key.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    node = karte.get_node('41049ad37a7606270568a519d50906c53403fc70')
    assert 1 == 1

