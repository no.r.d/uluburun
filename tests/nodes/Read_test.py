"""
Copyright 2022 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Map as Map
import json  # noqa: E402
import pytest  # noqa: E402, F401
import shutil
from hashlib import blake2s
from nord.nord import Runtime as RT
Runtime = RT.Runtime


def test_read_run_gen():
    try:
        m = json.load(open("tests/maps/test_gen_node_read.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_gen_node_read.n", "r"))
    karte = Map(m)
    vessal = Runtime()
    karte.run(vessal, None)
    # node = karte.get_node('7974c6b22feedb8e5d388f061535dfb894384338')
    assert 1 == 1


def test_write_read_data(cleanup_datastore):
    try:
        m = json.load(open("tests/maps/test_write_read_w_index.n", "r"))
    except FileNotFoundError:
        m = json.load(open("uluburun/tests/maps/test_write_read_w_index.n", "r"))
    karte = Map(m)
    # schedule the removal of the wiredtiger directory
    cleanup_datastore(lambda:  shutil.rmtree(f"{blake2s(karte.get_id().encode()).hexdigest()}"))

    vessal = Runtime()
    karte.run(vessal, None)

    assert vessal.get_anomaly() is None
    node = karte.get_node('4164b2d8-2748-4cdc-985f-fabf3d2ed1e1')
    assert node.value == 'Hey! This is some test data!'
